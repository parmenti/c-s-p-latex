#!/bin/bash

pdflatex bookexample.tex
bibtex ExOne
bibtex ExTwo
pdflatex bookexample.tex
pdflatex bookexample.tex
rm *.aux *.bbl *.blg *.toc *.out *.log
