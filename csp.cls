%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Unofficial LaTeX style
%% for Cambridge Scholar Publishing
%% Based on http://www.cambridgescholars.com/policies/M2%20Manuscript%20guidelines.pdf
%% and http://www.cambridgescholars.com/policies/M3%20Sample%20-%20format.pdf
%%
%% Author: Yannick Parmentier (yannick.parmentier@gmail.com)
%% Date: 2013/04/01
%% License: GPLv3 (see LICENSE file)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\NeedsTeXFormat{LaTeX2e}
 
\ProvidesClass{csp}[2012/10/05 Extension personnelle, V1.0]

\LoadClass[twoside,10pt]{book}
 
%%%%%%%%%%%%% PACKAGES %%%%%%%%%%%%
\RequirePackage[utf8]{inputenc}
\RequirePackage[T1]{fontenc}
\RequirePackage{lmodern}
\RequirePackage{graphicx}
\RequirePackage{indentfirst}
%\RequirePackage[english]{babel} %% PREVENTS HEADER REDEFINITION
\RequirePackage[left=21mm,right=21mm,top=25mm,bottom=15mm,paperwidth=148mm,paperheight=210mm]{geometry}
\RequirePackage{titlesec}
\RequirePackage{times}
\RequirePackage{mathptmx}
\RequirePackage{fmtcount}
\RequirePackage{fancyhdr}
\RequirePackage[sectionbib]{natbib}
\RequirePackage{multibib}
\RequirePackage{quoting}
\RequirePackage{breakcites}
\RequirePackage[font=small,tableposition=top,justification=justified,singlelinecheck=false]{caption}
\RequirePackage{float}
\floatstyle{plaintop}
\restylefloat{table}
\RequirePackage{color}
\RequirePackage{colortbl}
% The lipsum package is just used to generate dummy text
\RequirePackage{lipsum}
\RequirePackage{emptypage}
\RequirePackage[hang]{footmisc}
\RequirePackage{imakeidx}

\pagestyle{fancy}
\renewcommand{\contentsname}{Contents}
\renewcommand{\listfigurename}{List of Illustrations}
\linespread{1}
%%%%%%%%%%%%% INDENTATION %%%%%%%%%%%%%%
\setlength\parindent{.5cm}
%%%%%%%%%%%%% QUOTES %%%%%%%%%%%%%%%%%%%
\quotingsetup{vskip=10pt,leftmargin=.5cm,rightmargin=.5cm}
\AtBeginEnvironment{quote}{\small}
%%%%%%%%%%%%% SIZE COMMANDS %%%%%%%%%%%%
\newcommand{\titlesize}{\fontsize{16pt}{20pt}\selectfont}
\newcommand{\alevelsize}{\fontsize{12pt}{20pt}\selectfont}
\newcommand{\blevelsize}{\fontsize{11pt}{20pt}\selectfont}
\newcommand{\clevelsize}{\fontsize{10pt}{20pt}\selectfont}
\newcommand{\headersize}{\fontsize{9pt}{20pt}\selectfont}
%%%%%%%%%%%%% HEADERS %%%%%%%%%%%%
%% \makeatletter
%% \let\ps@plain\ps@empty
%% \makeatother
\renewcommand{\chaptermark}[1]{%
\ifnum\c@chapter>0
  \markboth{#1}{}%
\else
  \markboth{}{}%
\fi
}
\fancypagestyle{specialTOC}{%
\lhead{}
\rhead{}
\chead{}
\fancyhead[CE]{\sc {\headersize Constraints and Language}}
\fancyhead[CO]{\sc {\headersize \contentsname}} 
\renewcommand{\headrulewidth}{0pt}
\cfoot{{\headersize \thepage}}
\renewcommand{\footrulewidth}{0pt}
}
\fancypagestyle{specialLOF}{%
\lhead{}
\rhead{}
\chead{}
\fancyhead[CE]{\sc {\headersize Constraints and Language}}
\fancyhead[CO]{\sc {\headersize \listfigurename}} 
\renewcommand{\headrulewidth}{0pt}
\cfoot{{\headersize \thepage}}
\renewcommand{\footrulewidth}{0pt}
}
\fancypagestyle{specialLOT}{%
\lhead{}
\rhead{}
\chead{}
\fancyhead[CE]{\sc {\headersize Constraints and Language}}
\fancyhead[CO]{\sc {\headersize \listtablename}} 
\renewcommand{\headrulewidth}{0pt}
\cfoot{{\headersize \thepage}}
\renewcommand{\footrulewidth}{0pt}
}
\fancypagestyle{specialOTHER}{%
\lhead{}
\rhead{}
\chead{}
\fancyhead[CE]{\sc {\headersize Constraints and Language}}
\fancyhead[CO]{\sc {\headersize \leftmark}}
\renewcommand{\headrulewidth}{0pt}
\cfoot{{\headersize \thepage}}
\renewcommand{\footrulewidth}{0pt}
}
\fancypagestyle{specialCONTRIB}{%
\lhead{}
\rhead{}
\chead{}
\fancyhead[CE]{\sc {\headersize Constraints and Language}}
\fancyhead[CO]{\sc {\headersize \leftmark}}
\fancyhead[LE,RO]{\headersize \thepage}
\renewcommand{\headrulewidth}{0pt}
\cfoot{}
\renewcommand{\footrulewidth}{0pt}
}
\fancypagestyle{specialINDEX}{%
\lhead{}
\rhead{}
\chead{}
\fancyhead[CE]{\sc {\headersize Constraints and Language}}
\fancyhead[CO]{\sc {\headersize Index}}
\fancyhead[LE,RO]{\headersize \thepage}
\renewcommand{\headrulewidth}{0pt}
\cfoot{}
\renewcommand{\footrulewidth}{0pt}
}
\fancypagestyle{body}{%
\lhead{}
\rhead{}
\chead{}
\fancyhead[CE]{\sc {\headersize Chapter \Numberstringnum{\thechapter}}}
\fancyhead[CO]{\sc {\headersize \leftmark}}
\fancyhead[LE,RO]{\headersize \thepage}
%% HEADER %%
\renewcommand{\headrulewidth}{0pt}
\lfoot{}
\cfoot{}
\rfoot{}
\fancyfoot[LE,RO]{}
\renewcommand{\footrulewidth}{0pt}
}
%%%%%%%%%%%%% CAPTION SPACE %%%%%%%%%
\setlength{\abovecaptionskip}{8pt plus 3pt minus 2pt}
%%%%%%%%%%%%% FOOTNOTES INDENTATION %%%%%%%%
\renewcommand{\hangfootparindent}{2em}
\renewcommand{\footnotemargin}{1.em}
%%%%%%%%%%%%% TITLES %%%%%%%%%%%%
\titleformat{\section}%
{\centering\sc\alevelsize}{\thesection}{0.5em}{}
\titleformat{\subsection}%
{\centering\sc\blevelsize}{\thesubsection}{0.5em}{}
\titleformat{\subsubsection}%
{\centering\sc\clevelsize}{\thesubsubsection}{0.5em}{}
\titleformat{\part}%
[display]%
{\thispagestyle{empty}\huge\centering}{Part \thepart}{0.5em}{}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand{\chapterauthor}{}
\newcommand{\chaptercity}{}
\titleformat{\chapter}% command to format the chapter titles
        [display]% shape/type of title
        {\sc\titlesize}% formatting commands applied to both label and title
        {\centering Chapter \Numberstringnum{\thechapter}}% chapter number
        {10pt}% separation between number and chapter title; we've already covered this with the box
        {\centering}% additional formatting command for title itself not applied to number
        [% everything inside [...] below comes after the title
            \normalsize\normalfont% reset font formatting
            \vspace{\baselineskip}% add a vertical space before author
            %\hspace*{0.5in}% indent author name width of chapter number box
            \normalsize% make text that follows normal
            \thispagestyle{empty}% suppress page numbers
            \center{\textsc{\chapterauthor}}% insert chapter author name in large font
            \\% linebreak
            %\hspace*{0.5in}% again indent width of chapter number box
            \center{\textsc{\chaptercity}}% insert city
        ]% end of what comes after title
\titlespacing*{\chaptermark}% spacing commands for chapter titles; the star unindents first line afterwards
     {0em}% spacing to left of chapter title
     {0ex}% vertical space before title
     {3\baselineskip}% vertical spacing after title; here set to 3 lines
%%%%%%%%%%% INDEX HEADER %%%%%%%%%%%%
\indexsetup{toclevel=chapter}%,othercode={\pagestyle{specialINDEX}}}
\makeindex[program=makeindex,intoc=true,columns=2,options={-s idx_style.ist}]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
