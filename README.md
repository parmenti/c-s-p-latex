# c-s-p-latex

## description

c-s-p-latex is a LaTeX style file (csp.cls) together with a LaTeX example (bookexample.tex) of a collaborative book. It can be used to produce PDF monographs which are compliant with [Cambridge-Scholar-Publishing](http://www.cambridgescholars.com/) [guidelines](http://www.cambridgescholars.com/policies/M3%20Sample%20-%20format.pdf).

## License

c-s-p-latex is released under the terms of the GPL license v3 or higher.

## Requirements

c-s-p-latex uses other LaTeX styles, including [multibib](https://www.ctan.org/pkg/multibib) to handle multiple bibliographies (one per chapter).

## Usage

c-s-p-latex comes with a compilation script, illustrating how to use these style to compile a pdf book with pdflatex.
